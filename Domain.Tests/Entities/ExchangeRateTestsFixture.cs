using Bogus;
using Prova;

namespace Domain.Tests;

public class ExchangeRateTestsFixture
{
    public ExchangeRate GenerateExchangeRateWithFactorWithMoreThan5Decimals()
    {
        return new ExchangeRate(
                new DateOnly(2020,2,1),
                2.33333333m,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            );
    }

    public ExchangeRate GenerateExchangeRateWithFactorBeeingZeroOrLess()
    {
        return new Faker<ExchangeRate>("pt_BR").CustomInstantiator(f =>new ExchangeRate(
                new Faker().Date.RecentDateOnly(20),
                f.Random.Decimal(-3,0),
                f.Random.Guid(),
                f.Random.Guid(),
                f.Random.Guid()
            ));
    }
}