using Prova;

namespace Domain.Tests;

public class ExchangeRateTests : IClassFixture<ExchangeRateTestsFixture>
{
    private readonly ExchangeRateTestsFixture _fixture;

    public ExchangeRateTests(ExchangeRateTestsFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact(DisplayName = "Factor more than 5 decimals test")]
    public void Validate_FactorHasMoreThan5Decimals_ShouldReturn5Decimals()
    {
        var exchangeRate = _fixture.GenerateExchangeRateWithFactorWithMoreThan5Decimals();
        
        var count = BitConverter.GetBytes(decimal.GetBits(exchangeRate.Factor)[3])[2];

        Assert.Equal(5, count);
    }

    [Fact(DisplayName = "Factor is zero or less test")]
    public void Validate_FactorIsZeroOrLess_ShouldReturnException()
    {
        var exchangeRate = Record.Exception(() => _fixture.GenerateExchangeRateWithFactorBeeingZeroOrLess());        

        Assert.IsType<DomainException>(exchangeRate);
        Assert.Equivalent("The Factor cannot be zero or less", exchangeRate.Message);
    }

    [Fact(DisplayName = "TypeId is null test")]
    public void Validate_TypeIdIsNull_ShouldReturnException()
    {
        var exchangeRate = Record.Exception(() => new ExchangeRate(
                new DateOnly(2020,2,1),
                2.33333m,
                new Guid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            ));        

        Assert.IsType<DomainException>(exchangeRate);
        Assert.Equivalent("The TypeId cannot be empty", exchangeRate.Message);
    }

    [Fact(DisplayName = "QuotationDate is in the future")]
    public void Validate_QuotationDateIsInTheFuture_ShouldReturnException()
    {
        var exchangeRate = Record.Exception(() => new ExchangeRate(
                new DateOnly(2028,10,19),
                2.33333m,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            ));        

        Assert.IsType<DomainException>(exchangeRate);
        Assert.Equivalent("The QuotationDate cannot be in the future", exchangeRate.Message);
    }
}